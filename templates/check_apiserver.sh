#!/bin/sh

errorExit() {
    echo "*** $*" 1>&2
    exit 1
}

curl --silent --max-time 2 --insecure https://localhost:6443/ -o /dev/null || errorExit "Error GET https://localhost:6443/"
if ip addr | grep -q {{ hostvars[inventory_hostname].vip }}; then
    curl --silent --max-time 2 --insecure https://{{ hostvars[inventory_hostname].vip }}:6442/ -o /dev/null || errorExit "Error GET https://{{ hostvars[inventory_hostname].vip }}:6442/"
fi